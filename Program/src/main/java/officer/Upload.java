package officer;

import java.io.File;
import java.io.IOException;

import java.io.File;
import javax.servlet.http.Part;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.http.HttpSession;

import entity.ChkVehicle;
import entity.Vehicle;

public class Upload extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            req.getRequestDispatcher("/Officer/upload.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            String keyword = req.getParameter("keyword");
            Vehicle vehicle = new Vehicle();
            ChkVehicle chkVehicle = new ChkVehicle();
            vehicle.searchVehicle(keyword);
            chkVehicle.getChkVehicleByID(Integer.toString(vehicle.getID()));
            if (keyword.equals(""))
                vehicle = null;

            ServletContext context = req.getServletContext();
            String path = context.getRealPath("/");
            String fileName = "";
            ArrayList<String> pics = new ArrayList<>();
            for (int i = 1; i < 11; i++) {
                fileName = String.valueOf(path + "uploadDir/" + vehicle.getID() + "." + i + ".jpg");

                File checkfile = new File(fileName);
                if (checkfile.exists()) {
                    pics.add("uploadDir/" + vehicle.getID() + "." + i + ".jpg");

                } else {
                    pics.add("");

                }

            }

            if (keyword.equals("") || chkVehicle.getId() == 0)
                vehicle = null;

            req.setAttribute("fileName", fileName);
            req.setAttribute("pics", pics);
            req.setAttribute("vehicle", vehicle);
            req.setAttribute("chkVehicle", chkVehicle);
            req.setAttribute("keyword", keyword);

            req.getRequestDispatcher("/Officer/upload.jsp").forward(req, resp);
        }
    }
}
