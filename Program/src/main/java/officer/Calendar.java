package officer;

import java.io.File;
import java.io.IOException;
import java.time.YearMonth;
import java.io.File;
import javax.servlet.http.Part;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.http.HttpSession;

import entity.ChkVehicle;
import entity.Vehicle;

public class Calendar extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            YearMonth month = YearMonth.now();

            ServletContext context = req.getServletContext();
            String path = context.getRealPath("/");

            ArrayList<String> pics = new ArrayList<>();
            String fileName = String.valueOf(path + "uploadDir/" + month + ".jpg");

            File checkfile = new File(fileName);

            if (checkfile.exists()) {
                pics.add("uploadDir/" + month + ".jpg");

            } else {
                pics.add("");
            }

            req.setAttribute("pics", pics);
            req.setAttribute("month", month);
            req.getRequestDispatcher("/Officer/calendar.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            String month = req.getParameter("month");

            ServletContext context = req.getServletContext();
            String path = context.getRealPath("/");

            ArrayList<String> pics = new ArrayList<>();
            String fileName = String.valueOf(path + "uploadDir/" + month + ".jpg");

            File checkfile = new File(fileName);

            if (checkfile.exists()) {
                pics.add("uploadDir/" + month + ".jpg");

            } else {
                pics.add("");
            }

            req.setAttribute("pics", pics);
            req.setAttribute("month", month);
            req.getRequestDispatcher("/Officer/calendar.jsp").forward(req, resp);
        }
    }
}
