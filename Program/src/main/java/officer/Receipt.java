package officer;

import java.io.IOException;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Auction;
import entity.ChkVehicle;
import entity.SetTime;
import entity.User;
import entity.Vehicle;

public class Receipt extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            req.getRequestDispatcher("/Officer/receipt.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            Date today = new Date();

            Format formatter;
            formatter = new SimpleDateFormat("d MMM yy", new Locale("th", "TH"));

            // get user info
            String keyword = req.getParameter("keyword");
            User user = new User();
            user.getWinner(keyword);

            // get winning vehicle
            Auction auction = new Auction();
            ArrayList<Auction> auctions = auction.getWinVehicle(user.getUserid());

            // get openprice
            ArrayList<SetTime> settimes = new ArrayList<>();

            ArrayList<Vehicle> vehicles = new ArrayList<>();

            int totalPrice = 0;

            for (Auction auc : auctions) {
                Vehicle vehicle = new Vehicle();
                SetTime settime = new SetTime();
                vehicles.add(vehicle.getVehicleById(auc.getVehicleid()));
                settimes.add(settime.getSetTimeByVegicle(String.valueOf(auc.getVehicleid())));

                totalPrice += auc.getPriceauction().intValue();
            }

            // totalPrice formant
            NumberFormat priceformat = NumberFormat.getInstance(Locale.US);
            priceformat.setMinimumFractionDigits(2);

            for (SetTime st : settimes) {

            }

            req.setAttribute("today", formatter.format(today));
            req.setAttribute("keyword", keyword);
            req.setAttribute("user", user);
            req.setAttribute("auctions", auctions);
            req.setAttribute("settimes", settimes);
            req.setAttribute("vehicles", vehicles);
            req.setAttribute("totalPrice", priceformat.format(totalPrice));

            req.getRequestDispatcher("/payment/paymentForm.jsp").forward(req, resp);
        }
    }

}
