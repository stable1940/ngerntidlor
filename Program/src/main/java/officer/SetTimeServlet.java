package officer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.ChkVehicle;
import entity.SetTime;
import entity.Vehicle;

public class SetTimeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            req.getRequestDispatcher("/Officer/settime.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            String keyword = req.getParameter("keyword");
            Vehicle vehicle = new Vehicle();
            ChkVehicle chkVehicle = new ChkVehicle();
            SetTime setTime = new SetTime();
            vehicle.searchVehicle(keyword);
            chkVehicle.getChkVehicleByID(Integer.toString(vehicle.getID()));
            setTime.getSetTimeByVegicle(Integer.toString(vehicle.getID()));

            if (keyword.equals("") || chkVehicle.getId() == 0)
                vehicle = null;

            if (setTime.getId() == 0) {
                setTime.setOrdersell(chkVehicle.getOrdersell());
                setTime.setPricestart(chkVehicle.getPricestart());
            }

            req.setAttribute("vehicle", vehicle);
            req.setAttribute("chkVehicle", chkVehicle);
            req.setAttribute("setTime", setTime);
            req.setAttribute("keyword", keyword);

            req.getRequestDispatcher("/Officer/settime.jsp").forward(req, resp);
        }
    }
}
