package officer;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.jdbc.Driver;

import org.apache.catalina.User;

import config.DBConnection;

import com.fasterxml.jackson.databind.ObjectMapper;

import entity.ChkVehicle;
import entity.Vehicle;

public class ExportCsvServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            resp.setContentType("text/csv");
            resp.setHeader("Content-Disposition", "attachment; filename=\"userDirectory.csv\"");
            DBConnection dbConnection = DBConnection.getInstance();

            String[] param = new String[] {};
            String sql = "SELECT * FROM vehicle";
            ResultSet resultSet = dbConnection.dataQueryGet(sql, param);

            try {
                OutputStream outputStream = resp.getOutputStream();
                String outputResult = "ID, เลขที่สัญญา, ยี่ห้อ, รุ่น, สี, ปี, เลขทะเบียน, เลขทะเบียนจังหวัด, เลขเครื่องยนต์, เลขตัวถัง\n";
                while (resultSet.next()) {

                    outputResult += resultSet.getInt("ID") + "," + resultSet.getString("AgreementNo") + ","
                            + resultSet.getString("VehicleBrand") + "," + resultSet.getString("VehicleModel") + ","
                            + resultSet.getString("VehicleColor") + "," + resultSet.getInt("VehicleYear") + ","
                            + resultSet.getString("Registeration") + "," + resultSet.getString("RegisterationProvince")
                            + "," + resultSet.getString("AUTO_ENGINE_NO") + "," + resultSet.getString("AUTO_BODY_NO")
                            + "\n";
                }
                outputStream.write(outputResult.getBytes());
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }
}
