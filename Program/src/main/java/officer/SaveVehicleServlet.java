package officer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.ChkVehicle;
import entity.Vehicle;

public class SaveVehicleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            req.getRequestDispatcher("/Officer/checkvehicle.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {

            int agreementNo;
            int AUTO_ENGINE_NO;
            int AUTO_BODY_NO;

            if (req.getParameter("chk_AgreementNo") == null) {
                agreementNo = 0;
            } else {
                agreementNo = 1;
            }
            if (req.getParameter("chk_auto_engine_no") == null) {
                AUTO_ENGINE_NO = 0;
            } else {
                AUTO_ENGINE_NO = 1;
            }
            if (req.getParameter("chk_auto_body_no") == null) {
                AUTO_BODY_NO = 0;
            } else {
                AUTO_BODY_NO = 1;
            }

            int vehicleid = Integer.parseInt(req.getParameter("vehicleid").toString());
            String conditionreport = req.getParameter("conditionreport");
            int condition_vehicle = Integer.parseInt(req.getParameter("condition_vehicle").toString());
            int wheel = Integer.parseInt(req.getParameter("wheel").toString());
            int frontbreak = Integer.parseInt(req.getParameter("frontbreak").toString());
            int backbreak = Integer.parseInt(req.getParameter("backbreak").toString());
            int clutch = Integer.parseInt(req.getParameter("clutch").toString());
            int start = Integer.parseInt(req.getParameter("start").toString());
            int vehicle_key = Integer.parseInt(req.getParameter("vehicle_key").toString());

            String str_order = req.getParameter("ordersell").toString();
            int ordersell;
            if (!str_order.equals(""))
                ordersell = Integer.parseInt(req.getParameter("ordersell").toString());
            else
                ordersell = 0;
            BigDecimal pricestart;
            String str_pricestart = req.getParameter("pricestart").toString();
            if (!str_pricestart.equals(""))
                pricestart = BigDecimal.valueOf((Integer.parseInt(req.getParameter("pricestart").toString())));
            else
                pricestart = BigDecimal.valueOf(0);

            ChkVehicle chkVehicle = new ChkVehicle();
            String chk_id = req.getParameter("ID");
            if (!chk_id.equals("") && !chk_id.equals("0")) {

                int ID = Integer.parseInt(req.getParameter("ID").toString());
                chkVehicle.updateChkVehicle(ID, vehicleid, conditionreport, agreementNo, AUTO_ENGINE_NO, AUTO_BODY_NO,
                        condition_vehicle, wheel, frontbreak, backbreak, clutch, start, vehicle_key, ordersell,
                        pricestart);

            } else {

                chkVehicle.addChkVehicle(vehicleid, conditionreport, agreementNo, AUTO_ENGINE_NO, AUTO_BODY_NO,
                        condition_vehicle, wheel, frontbreak, backbreak, clutch, start, vehicle_key, ordersell,
                        pricestart);

            }
            String alertSave = "<script>alert('บันทึกสำเร็จ');</script>";
            req.setAttribute("alertSave", alertSave);
            req.getRequestDispatcher("/Officer/checkvehicle.jsp").forward(req, resp);
        }
    }
}
