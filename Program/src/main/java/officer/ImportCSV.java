package officer;

import java.io.File;
import java.io.IOException;

import java.io.File;
import javax.servlet.http.Part;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import com.opencsv.CSVReader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import javax.servlet.http.HttpSession;

import entity.ChkVehicle;
import entity.Vehicle;

@WebServlet("/importCSV")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class ImportCSV extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public static final String SAVE_DIRECTORY = "uploadDir";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            req.getRequestDispatcher("/Officer/importCSV.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            try {

                String appPath = req.getServletContext().getRealPath("");
                appPath = appPath.replace('\\', '/');

                // The directory to save uploaded file
                String fullSavePath = null;
                if (appPath.endsWith("/")) {
                    fullSavePath = appPath + SAVE_DIRECTORY;
                } else {
                    fullSavePath = appPath + "/" + SAVE_DIRECTORY;
                }

                // Creates the save directory if it does not exists
                File fileSaveDir = new File(fullSavePath);
                if (!fileSaveDir.exists()) {
                    fileSaveDir.mkdir();
                }

                // Part list (multi files).
                for (Part part : req.getParts()) {

                    // file name
                    String fileName = extractFileName(part);

                    if (fileName != null && fileName.length() > 0) {

                        fileName = String.valueOf("data.csv");

                        String filePath = fullSavePath + File.separator + fileName;

                        // delete file with same name if exist
                        File checkfile = new File(fileName);
                        if (checkfile.exists()) {
                            checkfile.delete();
                        }

                        part.write(filePath);

                    }

                }

                // Upload successfully!.
                resp.sendRedirect(req.getContextPath() + "/importcsv");

            } catch (Exception e) {
                e.printStackTrace();
                req.setAttribute("errorMessage", "Error: " + e.getMessage());
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Officer/importCSV.jsp");
                dispatcher.forward(req, resp);
            }

        }
    }

    private String extractFileName(Part part) {

        // form-data; name="file"; filename="C:\file1.zip"
        // form-data; name="file"; filename="C:\Note\file2.zip"
        // get filename in form
        String contentDisp = part.getHeader("content-disposition");

        // put in array
        String[] items = contentDisp.split(";");

        for (String s : items) {

            if (s.trim().startsWith("filename")) {

                // C:\file1.zip
                // C:\Note\file2.zip
                // get only file name and types
                String clientFileName = s.substring(s.indexOf("=") + 2, s.length() - 1);

                clientFileName = clientFileName.replace("\\", "/");

                int i = clientFileName.lastIndexOf('/');
                // file1.zip
                // file2.zip

                return clientFileName.substring(i + 1);

            }

        }

        return null;
    }
}
