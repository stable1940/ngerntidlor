package entity;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import config.DBConnection;

public class History {
    private int id;
    private String firstname;
    private String lastname;
    private String vehiclebrand;
    private String vehiclemodel;
    private String autobodyno;
    private String registeration;
    private String registerationprovince;
    private int priceauctionmax;
    private String statuswin;
    private String statusauction;
    private DBConnection dbConnection;

    public History() {
    }

    public History(int id, String vehiclebrand, String vehiclemodel, String autobodyno, String registeration,
            String registerationprovince, int priceauctionmax, String statuswin, String statusauction, String firstname,
            String lastname, DBConnection dbConnection) {
        this.id = id;
        this.vehiclebrand = vehiclebrand;
        this.vehiclemodel = vehiclemodel;
        this.autobodyno = autobodyno;
        this.registeration = registeration;
        this.registerationprovince = registerationprovince;
        this.priceauctionmax = priceauctionmax;
        this.statuswin = statuswin;
        this.statusauction = statusauction;
        this.firstname = firstname;
        this.lastname = lastname;
        this.dbConnection = dbConnection;
    }

    public ArrayList<History> getHistories(int userid) {
        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { Integer.toString(userid) };
        String sql = "select vehicle.ID,vehicle.VehicleBrand,vehicle.VehicleModel,vehicle.AUTO_BODY_NO,vehicle.Registeration,RegisterationProvince,maxprice.priceauctionmax,if(Now() > concat(settime.date_end,' ',settime.time_end),if(pricemax is not null,'ชนะ','แพ้'),'ยังไม่รู้ผล') statuswin,if(Now() > concat(settime.date_end,' ',settime.time_end),'จบประมูล','กำลังประมูล') statusauction from maxprice inner join vehicle on vehicle.ID = maxprice.vehicleid left join auction_view on maxprice.vehicleid=auction_view.vehicleid and auction_view.pricemax=maxprice.priceauctionmax left join settime on settime.vehicleid = maxprice.vehicleid where userid= ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        ArrayList<History> histories = new ArrayList<>();
        try {
            while (resultSet.next()) {
                History history = new History();
                history.setId(resultSet.getInt("ID"));
                history.setVehiclebrand(resultSet.getString("VehicleBrand"));
                history.setVehiclemodel(resultSet.getString("VehicleModel"));
                history.setAutobodyno(resultSet.getString("AUTO_BODY_NO"));
                history.setRegisteration(resultSet.getString("Registeration"));
                history.setRegisterationprovince(resultSet.getString("RegisterationProvince"));
                history.setPriceauctionmax(resultSet.getInt("priceauctionmax"));
                history.setStatuswin(resultSet.getString("statuswin"));
                history.setStatusauction(resultSet.getString("statusauction"));
                histories.add(history);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return histories;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehiclebrand() {
        return this.vehiclebrand;
    }

    public void setVehiclebrand(String vehiclebrand) {
        this.vehiclebrand = vehiclebrand;
    }

    public String getVehiclemodel() {
        return this.vehiclemodel;
    }

    public void setVehiclemodel(String vehiclemodel) {
        this.vehiclemodel = vehiclemodel;
    }

    public String getAutobodyno() {
        return this.autobodyno;
    }

    public void setAutobodyno(String autobodyno) {
        this.autobodyno = autobodyno;
    }

    public String getRegisteration() {
        return this.registeration;
    }

    public void setRegisteration(String registeration) {
        this.registeration = registeration;
    }

    public String getRegisterationprovince() {
        return this.registerationprovince;
    }

    public void setRegisterationprovince(String registerationprovince) {
        this.registerationprovince = registerationprovince;
    }

    public int getPriceauctionmax() {
        return this.priceauctionmax;
    }

    public void setPriceauctionmax(int priceauctionmax) {
        this.priceauctionmax = priceauctionmax;
    }

    public String getStatuswin() {
        return this.statuswin;
    }

    public void setStatuswin(String statuswin) {
        this.statuswin = statuswin;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStatusauction() {
        return this.statusauction;
    }

    public void setStatusauction(String statusauction) {
        this.statusauction = statusauction;
    }

    public DBConnection getDbConnection() {
        return this.dbConnection;
    }

    public void setDbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public History id(int id) {
        this.id = id;
        return this;
    }

    public History vehiclebrand(String vehiclebrand) {
        this.vehiclebrand = vehiclebrand;
        return this;
    }

    public History vehiclemodel(String vehiclemodel) {
        this.vehiclemodel = vehiclemodel;
        return this;
    }

    public History autobodyno(String autobodyno) {
        this.autobodyno = autobodyno;
        return this;
    }

    public History registeration(String registeration) {
        this.registeration = registeration;
        return this;
    }

    public History registerationprovince(String registerationprovince) {
        this.registerationprovince = registerationprovince;
        return this;
    }

    public History priceauctionmax(int priceauctionmax) {
        this.priceauctionmax = priceauctionmax;
        return this;
    }

    public History statuswin(String statuswin) {
        this.statuswin = statuswin;
        return this;
    }

    public History statusauction(String statusauction) {
        this.statusauction = statusauction;
        return this;
    }

    public History dbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
        return this;
    }
}
