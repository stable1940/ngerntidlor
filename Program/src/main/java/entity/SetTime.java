package entity;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import config.DBConnection;

public class SetTime {
    private int id;
    private int vehicleid;
    private String time_begin;
    private String time_end;
    private String date;
    private String date_end;
    private int ordersell;
    private BigDecimal pricestart;
    private DBConnection dbConnection;

    public SetTime() {

    }

    public void addSetTime(int vehicleid, String time_begin, String time_end, String date, String end_date,
            int ordersell, BigDecimal pricestart) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { Integer.toString(vehicleid), time_begin, time_end, date, end_date,
                Integer.toString(ordersell), pricestart.toString() };
        String sql = "INSERT INTO settime (vehicleid,time_begin,time_end,date,date_end,ordersell,pricestart) VALUE (?,?,?,?,?,?,?)";
        this.dbConnection.updateQuery(sql, param);
    }

    public void updateSetTime(int settimeid, int vehicleid, String time_begin, String time_end, String date,
            String end_date, int ordersell, BigDecimal pricestart) {
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { Integer.toString(vehicleid), time_begin, time_end, date, end_date,
                Integer.toString(ordersell), pricestart.toString(), Integer.toString(settimeid) };
        String sql = "UPDATE settime set vehicleid = ?, time_begin = ?,time_end = ?,date = ?,date_end=?,ordersell = ?,pricestart = ? WHERE id = ? ";
        this.dbConnection.updateQuery(sql, param);
    }

    public SetTime getSetTimeByVegicle(String vehicleid) {

        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { vehicleid };
        String sql = "SELECT * FROM settime WHERE vehicleid = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                ChkVehicle chkVehicle = new ChkVehicle();
                this.setId(resultSet.getInt("id"));
                this.setVehicleid(resultSet.getInt("vehicleid"));
                this.setTime_begin(resultSet.getString("time_begin"));
                this.setTime_end(resultSet.getString("time_end"));
                this.setDate(resultSet.getString("date"));
                this.setDate_end(resultSet.getString("date_end"));
                this.setOrdersell(resultSet.getInt("ordersell"));
                this.setPricestart(resultSet.getBigDecimal("pricestart"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public SetTime(int id, int vehicleid, String time_begin, String time_end, String date, int ordersell,
            BigDecimal pricestart) {
        this.id = id;
        this.vehicleid = vehicleid;
        this.time_begin = time_begin;
        this.time_end = time_end;
        this.date = date;
        this.ordersell = ordersell;
        this.pricestart = pricestart;
    }

    public String getTime_begin() {
        return this.time_begin;
    }

    public void setTime_begin(String time_begin) {
        this.time_begin = time_begin;
    }

    public String getTime_end() {
        return this.time_end;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVehicleid() {
        return this.vehicleid;
    }

    public void setVehicleid(int vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate_end() {
        return this.date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public int getOrdersell() {
        return this.ordersell;
    }

    public void setOrdersell(int ordersell) {
        this.ordersell = ordersell;
    }

    public BigDecimal getPricestart() {
        return this.pricestart;
    }

    public void setPricestart(BigDecimal pricestart) {
        this.pricestart = pricestart;
    }

    public SetTime id(int id) {
        this.id = id;
        return this;
    }

}