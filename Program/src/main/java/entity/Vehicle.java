package entity;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import config.DBConnection;

public class Vehicle {
    public int ID;
    public String AgreementNo;
    public String VehicleBrand;
    public String VehicleModel;
    public String VehicleYear;
    public String Registeration;
    public String AUTO_ENGINE_NO;
    public String AUTO_BODY_NO;
    public String Repodate;
    public String ProcessDescThai;
    public String ENGINE_TYPE_DESC;
    public String PhysicalMile;
    public String VehicleColor;
    public String RegisterationProvince;
    public BigDecimal pricemax;
    private DBConnection dbConnection;

    public Vehicle getVehicleById(int id) {
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { Integer.toString(id) };
        String sql = "SELECT * FROM vehicle WHERE ID = ?  ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                this.setID(resultSet.getInt("ID"));
                this.setAgreementNo(resultSet.getString("AgreementNo"));
                this.setVehicleBrand(resultSet.getString("VehicleBrand"));
                this.setVehicleModel(resultSet.getString("VehicleModel"));
                this.setVehicleYear(resultSet.getString("VehicleYear"));
                this.setRegisteration(resultSet.getString("Registeration"));
                this.setAUTO_ENGINE_NO(resultSet.getString("AUTO_ENGINE_NO"));
                this.setAUTO_BODY_NO(resultSet.getString("AUTO_BODY_NO"));
                this.setRepodate(resultSet.getString("RepoDate"));
                this.setProcessDescThai(resultSet.getString("ProcessDescThai"));
                this.setENGINE_TYPE_DESC(resultSet.getString("ENGINE_TYPE_DESC"));
                this.setPhysicalMile(resultSet.getString("PhysicalMile"));
                this.setVehicleColor(resultSet.getString("VehicleColor"));
                this.setRegisterationProvince(resultSet.getString("RegisterationProvince"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;

    }

    // get count all vehicle
    public int getCountVehicle() {
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] {};
        String sql = "SELECT count(*) as count_vehicle FROM vehicle";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        int countvehicle = 0;
        try {
            resultSet.next();
            countvehicle = resultSet.getInt("count_vehicle");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countvehicle;
    }

    // get count where
    public int getCountVehicleWhere(String brand, String vehicle_model, String year) {
        this.dbConnection = DBConnection.getInstance();
        String vehicle_model1 = "%" + vehicle_model + "%";

        String[] param = new String[] {};
        String sql = "SELECT count(*) as count_vehicle FROM vehicle inner join settime on settime.vehicleid = vehicle.ID  WHERE 1";

        if (!brand.equals("")) {
            sql += " AND BRAND = '" + brand + "' ";
        }
        if (!vehicle_model.equals("")) {
            sql += " AND VehicleModel like '" + vehicle_model1 + "' ";
        }
        if (!year.equals("")) {
            sql += " AND VehicleYear= '" + year + "' ";
        }

        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        int countvehicle = 0;
        try {
            resultSet.next();
            countvehicle = resultSet.getInt("count_vehicle");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countvehicle;
    }

    public ArrayList<Vehicle> getAllVehicle() {
        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] {};

        String sql = "SELECT * FROM vehicle inner join settime on settime.vehicleid = vehicle.ID LEFT JOIN auction_view on vehicle.ID = auction_view.vehicleid";

        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        try {
            while (resultSet.next()) {
                Vehicle vehicle = new Vehicle();
                vehicle.setID(resultSet.getInt("ID"));
                vehicle.setAgreementNo(resultSet.getString("AgreementNo"));
                vehicle.setVehicleBrand(resultSet.getString("VehicleBrand"));
                vehicle.setVehicleModel(resultSet.getString("VehicleModel"));
                vehicle.setVehicleYear(resultSet.getString("VehicleYear"));
                vehicle.setRegisteration(resultSet.getString("Registeration"));
                vehicle.setAUTO_ENGINE_NO(resultSet.getString("AUTO_ENGINE_NO"));
                vehicle.setAUTO_BODY_NO(resultSet.getString("AUTO_BODY_NO"));
                vehicle.setRepodate(resultSet.getString("RepoDate"));
                vehicle.setProcessDescThai(resultSet.getString("ProcessDescThai"));
                vehicle.setENGINE_TYPE_DESC(resultSet.getString("ENGINE_TYPE_DESC"));
                vehicle.setPhysicalMile(resultSet.getString("PhysicalMile"));
                vehicle.setVehicleColor(resultSet.getString("VehicleColor"));
                vehicle.setRegisterationProvince(resultSet.getString("RegisterationProvince"));
                vehicle.setPricemax(resultSet.getBigDecimal("pricemax"));
                vehicles.add(vehicle);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vehicles;
    }

    public Vehicle searchVehicle(String keyword) {
        this.dbConnection = DBConnection.getInstance();
        String keyword2 = "%" + keyword + "%";
        String[] param = new String[] { keyword2, keyword, keyword, keyword };
        String sql = "SELECT * FROM vehicle WHERE Registeration like ? or AgreementNo = ? or AUTO_ENGINE_NO = ? or AUTO_BODY_NO = ? ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                this.setID(resultSet.getInt("ID"));
                this.setAgreementNo(resultSet.getString("AgreementNo"));
                this.setVehicleBrand(resultSet.getString("VehicleBrand"));
                this.setVehicleModel(resultSet.getString("VehicleModel"));
                this.setVehicleYear(resultSet.getString("VehicleYear"));
                this.setRegisteration(resultSet.getString("Registeration"));
                this.setAUTO_ENGINE_NO(resultSet.getString("AUTO_ENGINE_NO"));
                this.setAUTO_BODY_NO(resultSet.getString("AUTO_BODY_NO"));
                this.setRepodate(resultSet.getString("RepoDate"));
                this.setProcessDescThai(resultSet.getString("ProcessDescThai"));
                this.setENGINE_TYPE_DESC(resultSet.getString("ENGINE_TYPE_DESC"));
                this.setPhysicalMile(resultSet.getString("PhysicalMile"));
                this.setVehicleColor(resultSet.getString("VehicleColor"));
                this.setRegisterationProvince(resultSet.getString("RegisterationProvince"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    // public ArrayList<Vehicle> searchVehicleCatalog(String brand, String
    // vehicle_model, String year) {

    // this.dbConnection = DBConnection.getInstance();
    // String vehicle_model1 = "%" + vehicle_model + "%";
    // String[] param = new String[] {};

    // String sql = "SELECT * FROM vehicle WHERE 1";

    // if (!brand.equals("")) {
    // sql += " AND BRAND = '" + brand + "' ";
    // }
    // if (!vehicle_model.equals("")) {
    // sql += " AND VehicleModel like '" + vehicle_model1 + "' ";
    // }
    // if (!year.equals("")) {
    // sql += " AND VehicleYear= '" + year + "' ";
    // }

    // ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
    // ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
    // try {
    // while (resultSet.next()) {
    // Vehicle vehicle = new Vehicle();
    // vehicle.setID(resultSet.getInt("ID"));
    // vehicle.setAgreementNo(resultSet.getString("AgreementNo"));
    // vehicle.setVehicleBrand(resultSet.getString("VehicleBrand"));
    // vehicle.setVehicleModel(resultSet.getString("VehicleModel"));
    // vehicle.setVehicleYear(resultSet.getString("VehicleYear"));
    // vehicle.setRegisteration(resultSet.getString("Registeration"));
    // vehicle.setAUTO_ENGINE_NO(resultSet.getString("AUTO_ENGINE_NO"));
    // vehicle.setAUTO_BODY_NO(resultSet.getString("AUTO_BODY_NO"));
    // vehicle.setRepodate(resultSet.getString("RepoDate"));
    // vehicle.setProcessDescThai(resultSet.getString("ProcessDescThai"));
    // vehicle.setENGINE_TYPE_DESC(resultSet.getString("ENGINE_TYPE_DESC"));
    // vehicle.setPhysicalMile(resultSet.getString("PhysicalMile"));
    // vehicle.setVehicleColor(resultSet.getString("VehicleColor"));
    // vehicle.setRegisterationProvince(resultSet.getString("RegisterationProvince"));

    // vehicles.add(vehicle);
    // }
    // } catch (SQLException e) {
    // e.printStackTrace();
    // }
    // return vehicles;
    // }

    public ArrayList<Vehicle> searchVehicleCatalog(String brand, String vehicle_model, String year) {

        this.dbConnection = DBConnection.getInstance();
        String vehicle_model1 = "%" + vehicle_model + "%";
        String[] param = new String[] {};

        String sql = "SELECT * FROM vehicle inner join settime on settime.vehicleid = vehicle.ID LEFT JOIN auction_view on vehicle.ID = auction_view.vehicleid WHERE 1";

        if (!brand.equals("")) {
            sql += " AND BRAND = '" + brand + "' ";
        }
        if (!vehicle_model.equals("")) {
            sql += " AND VehicleModel like '" + vehicle_model1 + "' ";
        }
        if (!year.equals("")) {
            sql += " AND VehicleYear= '" + year + "' ";
        }

        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        try {
            while (resultSet.next()) {
                Vehicle vehicle = new Vehicle();
                vehicle.setID(resultSet.getInt("ID"));
                vehicle.setAgreementNo(resultSet.getString("AgreementNo"));
                vehicle.setVehicleBrand(resultSet.getString("VehicleBrand"));
                vehicle.setVehicleModel(resultSet.getString("VehicleModel"));
                vehicle.setVehicleYear(resultSet.getString("VehicleYear"));
                vehicle.setRegisteration(resultSet.getString("Registeration"));
                vehicle.setAUTO_ENGINE_NO(resultSet.getString("AUTO_ENGINE_NO"));
                vehicle.setAUTO_BODY_NO(resultSet.getString("AUTO_BODY_NO"));
                vehicle.setRepodate(resultSet.getString("RepoDate"));
                vehicle.setProcessDescThai(resultSet.getString("ProcessDescThai"));
                vehicle.setENGINE_TYPE_DESC(resultSet.getString("ENGINE_TYPE_DESC"));
                vehicle.setPhysicalMile(resultSet.getString("PhysicalMile"));
                vehicle.setVehicleColor(resultSet.getString("VehicleColor"));
                vehicle.setRegisterationProvince(resultSet.getString("RegisterationProvince"));
                vehicle.setPricemax(resultSet.getBigDecimal("pricemax"));

                vehicles.add(vehicle);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vehicles;
    }

    public ArrayList<String> getAllBrand() {
        ArrayList<String> brands = new ArrayList<>();
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] {};
        String sql = "SELECT brand FROM brand_model GROUP BY brand ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);

        try {
            while (resultSet.next()) {
                brands.add(resultSet.getString("brand"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return brands;
    }

    public ArrayList<String> getAllModel() {

        ArrayList<String> vehecle_models = new ArrayList<>();
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] {};
        String sql = "SELECT model FROM brand_model";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                vehecle_models.add(resultSet.getString("model"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vehecle_models;
    }

    public ArrayList<String> getAllYear() {

        ArrayList<String> years = new ArrayList<>();
        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] {};
        String sql = "SELECT VehicleYear FROM vehicle WHERE VehicleYear IS NOT NULL GROUP BY VehicleYear ORDER BY VehicleYear";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                years.add(resultSet.getString("VehicleYear"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return years;
    }

    public Vehicle() {
    }

    public Vehicle(int ID, String AgreementNo, String VehicleBrand, String VehicleModel, String VehicleYear,
            String Registeration, String AUTO_ENGINE_NO, String AUTO_BODY_NO, String Repodate, String ProcessDescThai,
            String ENGINE_TYPE_DESC, String PhysicalMile, String VehicleColor, String RegisterationProvince,
            BigDecimal pricemax, DBConnection dbConnection) {
        this.ID = ID;
        this.AgreementNo = AgreementNo;
        this.VehicleBrand = VehicleBrand;
        this.VehicleModel = VehicleModel;
        this.VehicleYear = VehicleYear;
        this.Registeration = Registeration;
        this.AUTO_ENGINE_NO = AUTO_ENGINE_NO;
        this.AUTO_BODY_NO = AUTO_BODY_NO;
        this.Repodate = Repodate;
        this.ProcessDescThai = ProcessDescThai;
        this.ENGINE_TYPE_DESC = ENGINE_TYPE_DESC;
        this.PhysicalMile = PhysicalMile;
        this.VehicleColor = VehicleColor;
        this.RegisterationProvince = RegisterationProvince;
        this.pricemax = pricemax;
        this.dbConnection = dbConnection;
    }

    public int getID() {
        return this.ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getAgreementNo() {
        return this.AgreementNo;
    }

    public void setAgreementNo(String AgreementNo) {
        this.AgreementNo = AgreementNo;
    }

    public String getVehicleBrand() {
        return this.VehicleBrand;
    }

    public void setVehicleBrand(String VehicleBrand) {
        this.VehicleBrand = VehicleBrand;
    }

    public String getVehicleModel() {
        return this.VehicleModel;
    }

    public void setVehicleModel(String VehicleModel) {
        this.VehicleModel = VehicleModel;
    }

    public String getVehicleYear() {
        return this.VehicleYear;
    }

    public void setVehicleYear(String VehicleYear) {
        this.VehicleYear = VehicleYear;
    }

    public String getRegisteration() {
        return this.Registeration;
    }

    public void setRegisteration(String Registeration) {
        this.Registeration = Registeration;
    }

    public String getAUTO_ENGINE_NO() {
        return this.AUTO_ENGINE_NO;
    }

    public void setAUTO_ENGINE_NO(String AUTO_ENGINE_NO) {
        this.AUTO_ENGINE_NO = AUTO_ENGINE_NO;
    }

    public String getAUTO_BODY_NO() {
        return this.AUTO_BODY_NO;
    }

    public void setAUTO_BODY_NO(String AUTO_BODY_NO) {
        this.AUTO_BODY_NO = AUTO_BODY_NO;
    }

    public String getRepodate() {
        return this.Repodate;
    }

    public void setRepodate(String Repodate) {
        this.Repodate = Repodate;
    }

    public String getProcessDescThai() {
        return this.ProcessDescThai;
    }

    public void setProcessDescThai(String ProcessDescThai) {
        this.ProcessDescThai = ProcessDescThai;
    }

    public String getENGINE_TYPE_DESC() {
        return this.ENGINE_TYPE_DESC;
    }

    public void setENGINE_TYPE_DESC(String ENGINE_TYPE_DESC) {
        this.ENGINE_TYPE_DESC = ENGINE_TYPE_DESC;
    }

    public String getPhysicalMile() {
        return this.PhysicalMile;
    }

    public void setPhysicalMile(String PhysicalMile) {
        this.PhysicalMile = PhysicalMile;
    }

    public String getVehicleColor() {
        return this.VehicleColor;
    }

    public void setVehicleColor(String VehicleColor) {
        this.VehicleColor = VehicleColor;
    }

    public String getRegisterationProvince() {
        return this.RegisterationProvince;
    }

    public void setRegisterationProvince(String RegisterationProvince) {
        this.RegisterationProvince = RegisterationProvince;
    }

    public BigDecimal getPricemax() {
        return this.pricemax;
    }

    public void setPricemax(BigDecimal pricemax) {
        this.pricemax = pricemax;
    }

    public DBConnection getDbConnection() {
        return this.dbConnection;
    }

    public void setDbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public Vehicle ID(int ID) {
        this.ID = ID;
        return this;
    }

    public Vehicle AgreementNo(String AgreementNo) {
        this.AgreementNo = AgreementNo;
        return this;
    }

    public Vehicle VehicleBrand(String VehicleBrand) {
        this.VehicleBrand = VehicleBrand;
        return this;
    }

    public Vehicle VehicleModel(String VehicleModel) {
        this.VehicleModel = VehicleModel;
        return this;
    }

    public Vehicle VehicleYear(String VehicleYear) {
        this.VehicleYear = VehicleYear;
        return this;
    }

    public Vehicle Registeration(String Registeration) {
        this.Registeration = Registeration;
        return this;
    }

    public Vehicle AUTO_ENGINE_NO(String AUTO_ENGINE_NO) {
        this.AUTO_ENGINE_NO = AUTO_ENGINE_NO;
        return this;
    }

    public Vehicle AUTO_BODY_NO(String AUTO_BODY_NO) {
        this.AUTO_BODY_NO = AUTO_BODY_NO;
        return this;
    }

    public Vehicle Repodate(String Repodate) {
        this.Repodate = Repodate;
        return this;
    }

    public Vehicle ProcessDescThai(String ProcessDescThai) {
        this.ProcessDescThai = ProcessDescThai;
        return this;
    }

    public Vehicle ENGINE_TYPE_DESC(String ENGINE_TYPE_DESC) {
        this.ENGINE_TYPE_DESC = ENGINE_TYPE_DESC;
        return this;
    }

    public Vehicle PhysicalMile(String PhysicalMile) {
        this.PhysicalMile = PhysicalMile;
        return this;
    }

    public Vehicle VehicleColor(String VehicleColor) {
        this.VehicleColor = VehicleColor;
        return this;
    }

    public Vehicle RegisterationProvince(String RegisterationProvince) {
        this.RegisterationProvince = RegisterationProvince;
        return this;
    }

    public Vehicle pricemax(BigDecimal pricemax) {
        this.pricemax = pricemax;
        return this;
    }

    public Vehicle dbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
        return this;
    }

}