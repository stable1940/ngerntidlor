package entity;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import config.DBConnection;

public class Auction {
    private int id;
    private int vehicleid;
    private int userid;
    private BigDecimal priceauction;
    private DBConnection dbConnection;

    public Auction() {
    }

    public Auction(int id, int vehicleid, int userid, BigDecimal priceauction) {
        this.id = id;
        this.vehicleid = vehicleid;
        this.userid = userid;
        this.priceauction = priceauction;
    }

    // public ArrayList<Auction> getMaxPrice() {
    // this.dbConnection = DBConnection.getInstance();

    // String[] param = new String[] {};
    // String sql = "Select auction.priceauction ,auction.vehicleid,auction.userid
    // from auction inner join auction_view on auction.vehicleid =
    // auction_view.vehicleid and auction.priceauction = auction_view.pricemax inner
    // join settime on settime.vehicleid = auction.vehicleid WHERE Now() >
    // concat(settime.date,' ',settime.time_end) group by auction.vehicleid order by
    // auction.id";
    // ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
    // ArrayList<Auction> auctions = new ArrayList<Auction>();
    // try {
    // while (resultSet.next()) {
    // Auction auction = new Auction();
    // auction.setVehicleid(resultSet.getInt("vehicleid"));
    // auction.setPriceauction(resultSet.getBigDecimal("priceauction"));

    // auctions.add(auction);

    // }
    // } catch (SQLException e) {
    // e.printStackTrace();
    // }
    // return auctions;
    // }

    public Auction getMaxPrice(int vehicleid) {
        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { Integer.toString(vehicleid) };
        String sql = "SELECT MAX(priceauction) FROM auction WHERE vehicleid = ? ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {

                this.setPriceauction(resultSet.getBigDecimal("priceauction"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public void addAuction(int vehicleid, int userid, BigDecimal priceauction) {

        this.dbConnection = DBConnection.getInstance();
        String[] param = new String[] { Integer.toString(vehicleid), Integer.toString(userid),
                priceauction.toString() };
        String sql = "INSERT INTO auction (vehicleid,userid,priceauction) VALUE (?,?,?)";
        this.dbConnection.updateQuery(sql, param);
    }

    public Auction getAuctionUserVehicle(int vehicleid, int userid) {
        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { Integer.toString(vehicleid), Integer.toString(userid) };
        String sql = "SELECT * FROM auction WHERE vehicleid = ? and userid = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                ChkVehicle chkVehicle = new ChkVehicle();
                this.setId(resultSet.getInt("id"));
                this.setVehicleid(resultSet.getInt("vehicleid"));
                this.setUserid(resultSet.getInt("userid"));
                this.setPriceauction(resultSet.getBigDecimal("priceauction"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public Auction getAlreadySettime(int userid) {
        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { Integer.toString(userid) };
        String sql = "select maxprice.* ,if(pricemax is not null,'win','lose') statuswin,settime.* from maxprice  inner join vehicle on vehicle.ID = maxprice.vehicleid left join auction_view on maxprice.vehicleid=auction_view.vehicleid and auction_view.pricemax=maxprice.priceauctionmaxleft join settime on settime.vehicleid = maxprice.vehicleid WHERE userid = ?";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        try {
            while (resultSet.next()) {
                ChkVehicle chkVehicle = new ChkVehicle();
                this.setId(resultSet.getInt("id"));
                this.setVehicleid(resultSet.getInt("vehicleid"));
                this.setUserid(resultSet.getInt("userid"));
                this.setPriceauction(resultSet.getBigDecimal("priceauction"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public int countAuction(int vehicleid) {
        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { Integer.toString(vehicleid) };
        String sql = "SELECT count(*) countauction FROM auction WHERE vehicleid = ? ";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        int count = 0;
        try {
            while (resultSet.next()) {
                count = resultSet.getInt("countauction");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public ArrayList<Auction> getWinVehicle(int userid) {
        this.dbConnection = DBConnection.getInstance();

        String[] param = new String[] { Integer.toString(userid) };
        String sql = "Select auction.priceauction ,auction.vehicleid,auction.userid from auction inner join auction_view on auction.vehicleid = auction_view.vehicleid and auction.priceauction = auction_view.pricemax  inner join settime on settime.vehicleid = auction.vehicleid WHERE Now() > concat(settime.date_end,' ',settime.time_end) AND userid = ? group by auction.vehicleid order by auction.id";
        ResultSet resultSet = this.dbConnection.dataQueryGet(sql, param);
        ArrayList<Auction> auctions = new ArrayList<Auction>();
        try {
            while (resultSet.next()) {
                Auction auction = new Auction();
                auction.setVehicleid(resultSet.getInt("vehicleid"));
                auction.setPriceauction(resultSet.getBigDecimal("priceauction"));

                auctions.add(auction);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return auctions;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVehicleid() {
        return this.vehicleid;
    }

    public void setVehicleid(int vehicleid) {
        this.vehicleid = vehicleid;
    }

    public int getUserid() {
        return this.userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public BigDecimal getPriceauction() {
        return this.priceauction;
    }

    public void setPriceauction(BigDecimal priceauction) {
        this.priceauction = priceauction;
    }

}