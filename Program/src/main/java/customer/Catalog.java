package customer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.ChkVehicle;
import entity.SetTime;
import entity.User;
import entity.Vehicle;

public class Catalog extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            Vehicle vehicle = new Vehicle();
            ServletContext context = req.getServletContext();
            String path = context.getRealPath("/");

            String id = req.getParameter("id");
            vehicle.getVehicleById(Integer.parseInt(id));

            String fileName = "";
            ArrayList<String> pics = new ArrayList<>();
            for (int i = 1; i < 11; i++) {
                fileName = String.valueOf(path + "uploadDir/" + vehicle.getID() + "." + i + ".jpg");

                File checkfile = new File(fileName);
                if (checkfile.exists()) {
                    pics.add("uploadDir/" + vehicle.getID() + "." + i + ".jpg");

                }

            }

            ChkVehicle chkVehicle = new ChkVehicle();
            chkVehicle.getChkVehicleByID(Integer.toString(vehicle.getID()));

            req.setAttribute("fileName", fileName);
            req.setAttribute("pics", pics);

            SetTime setTime = new SetTime();
            setTime.getSetTimeByVegicle(id);
            vehicle.getVehicleById(Integer.parseInt(id));
            // ArrayList<Vehicle> vehicles = vehicle.getAllVehicle();

            req.setAttribute("chkVehicle", chkVehicle);
            req.setAttribute("setTime", setTime);
            req.setAttribute("vehicle", vehicle);
            req.getRequestDispatcher("/LoginNgerntidlor/catalog.jsp").forward(req, resp);
        }
    }
}