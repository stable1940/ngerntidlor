package customer;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Auction;
import entity.AuctionCounter;
import entity.SetTime;
import entity.User;
import entity.Vehicle;

public class AuctionSave extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // can input Thai character
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        if (session.getAttribute("id") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            Vehicle vehicle = new Vehicle();
            String vehicleid = req.getParameter("vehicleid");
            String userid = session.getAttribute("id").toString();

            Auction auction = new Auction();
            auction.addAuction(Integer.parseInt(req.getParameter("vehicleid")),
                    Integer.parseInt(session.getAttribute("id").toString()),
                    BigDecimal.valueOf(Integer.parseInt(req.getParameter("priceauction").toString())));

            String id = req.getParameter("vehicleid");

            vehicle.getVehicleById(Integer.parseInt(id));
            SetTime setTime = new SetTime();
            setTime.getSetTimeByVegicle(id);
            vehicle.getVehicleById(Integer.parseInt(id));

            AuctionCounter auctionCounter = new AuctionCounter();
            int counter = auctionCounter.viewCounter(Integer.parseInt(id));

            // ArrayList<Vehicle> vehicles = vehicle.getAllVehicle();
            auction.getAuctionUserVehicle(Integer.parseInt(id),
                    Integer.parseInt(session.getAttribute("id").toString()));

            if (auction.getId() == 0) {
                auction.setPriceauction(setTime.getPricestart());
            }
            int countauction = 0;
            if (auction.countAuction(Integer.parseInt(id)) > 0)
                countauction = auction.countAuction(Integer.parseInt(id));

            req.setAttribute("vehicle", vehicle);
            req.setAttribute("auction", auction);
            req.setAttribute("counter", counter);
            req.setAttribute("countauction", countauction);
            req.setAttribute("setTime", setTime);
            req.getRequestDispatcher("/LoginNgerntidlor/AuctionForm.jsp").forward(req, resp);
            
        }
    }
}