﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>

<head>

    <div w3-include-html="../include/link.jsp"></div>
    <title>Payment Form</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/css/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            height: 30px;
        }

        span {
            font-size: 20px;

        }
    </style>

</head>

<body>
    <br><br>
    <div class="wholepage" style="margin: 0px 30px 0px 30px;">

        <div class="container" style="margin: 0">
            <div class="row" style="width: 100%">
                <div class="col-12">
                    <img src="payment/image1.jpeg" width="100%">
                </div>
                <div class="col-12">
                    <h5 style="text-align: center;">รายการที่ประมูลได้/ใบเสร็จรับเงินชั่วคราว</h5>
                </div>
                <div class="col-4">

                    <p>ชื่อ-สกุล ผู้ประมูลได้ </p>
                </div>
                <div class="col-4">

                    <p>${user.getFirstname()} ${user.getLastname()}</p>
                </div>
                <div class="col-4" style="text-align: right">
                    <p>${today}</p>
                </div>

            </div>
        </div>

        <table style="width:100%; height: 200px">
            <tr style="text-align: center">
                <th width="50">คันที่</th>
                <!-- <th width="25">จำนวนคัน</th> -->
                <th width="70">ยี่ห้อ</th>
                <th width="150">รุ่น</th>
                <th width="200">เลขตัวรถ</th>
                <th width="100">ทะเบียน</th>
                <th width="100">ชื่อผู้ที่ชนะการประมูล</th>
                <th width="100">ราคากลาง</th>
                <th width="100">ราคาประมูลสูงสุด</th>
            </tr>
            <c:forEach var="vehicle" items="${vehicles}" begin="0" varStatus="Index">
                <tr style="text-align: center">
                    <td>
                        <c:out value="${Index.count}" />
                    </td>
                    <!-- <td>1</td> -->
                    <td>
                        <c:out value="${vehicle.getVehicleBrand()}" />
                    </td>
                    <td>
                        <c:out value="${vehicle.getVehicleModel()}" />
                    </td>
                    <td>
                        <c:out value="${vehicle.getAUTO_BODY_NO()}" />
                    </td>
                    <td>
                        <c:out value="${vehicle.getRegisteration()}" />
                    </td>

                    <td>${user.getFirstname()} ${user.getLastname()}</td>

                    <td>
                        <!-- <c:out value="${settimes[Index.count-1].getPricestart()}" /> -->
                        <fmt:formatNumber pattern="#,##0.00" value="${settimes[Index.count-1].getPricestart()}" />

                    </td>
                    <td>
                        <!-- <c:out value="${auctions[Index.count-1].getPriceauction()}" /> -->
                        <fmt:formatNumber pattern="#,##0.00" value="${auctions[Index.count-1].getPriceauction()}" />

                    </td>
                </tr>
            </c:forEach>
            <c:forEach var="i" begin="1" end="${ 11 - vehicles.size()}">
                <tr style="text-align: center">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <!-- <td></td> -->
                </tr>
            </c:forEach>

        </table>

        <div>
            จำนวนคัน &nbsp;${vehicles.size()}
        </div>
        <br>

        <div class="row">
            <div class="col-1"></div>
            <div class="col-9">
                <p>จำนวนเงินที่ต้องชำระ</p>
            </div>
            <div class="col-2" style="text-align: center">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${totalPrice}</p>
            </div>

        </div>

        <div class="row">
            <div class="col-1"></div>
            <div class="col-9">
                <p>จำนวนเงินที่ชำระแล้ว</p>
            </div>
            <div class="col-2">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;___________</p>
            </div>

        </div>

        <div class="row">
            <div class="col-1"></div>
            <div class="col-9">
                <p>จำนวนเงินคงเหลือที่ต้องชำระ</p>
            </div>
            <div class="col-2">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;___________</p>
            </div>

        </div>

        <div class="row">
            <div class="col-1"></div>
            <div class="col-9">
                <p>ชำระทั้งหมด</p>
            </div>
            <div class="col-2">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;___________</p>
            </div>

        </div>

        <div class="row">
            <div class="col-1"></div>
            <div class="col-11">
                <strong>ข้าพเจ้าผู้เสนอราคาประมูล
                    ขอยืนยันราคาที่ข้าพเจ้าระบุไว้เป็นรายคันในใบเสร็จรับเงินชั่วคราวใบนี้</strong>
            </div>

        </div>

        <br><br>

        <div class="container">
            <div class="row">

                <div class="col-6" style="text-align: center">
                    <p>ลงชื่อ......................................................................</p>
                    <p>ผู้ชำระเงิน</p>
                </div>
                <div class="col-6" style="text-align: center">
                    <p>ลงชื่อ......................................................................</p>
                    <p>ผู้รับเงิน</p>
                </div>

            </div>
        </div>
        <br />
        <p>หมายเหตุ : ผู้ประมูลได้จะต้องดำเนินการโอนกรรมสิทธิ์ทางทะเบียนให้แล้วเสร็จ ภายใน 45 วัน นับจากวันประมูลได้
        </p>
        <p>หากรถไม่สามารถโอนกรรมสิทธิ์ได้ ไม่ว่าจะด้วยกรณีใด ๆ อันมิใช่ความผิดของผู้ประมูล บริษัทจะคืนเงินให้ตามราคา
        </p>
        <p>ที่เสนอซื้อรายคัน ยกเว้น รถที่ประมูลขายซาก เป็นอะไหล่ และรถที่ระบุในหมายเหตุ ทางบริษัทฯ จะไม่รับผิดชอบ
        </p>
        <p>ในความเสียหายที่เกิด ขึ้นไม่ว่ากรณีใดๆทั้งสิ้น</p>
        <p>ธนาคาร กรุงศรีอยุธยา ชื่อบัญชี บริษัท ซีเอฟจี เซอร์วิส จำกัด หรือ บริษัท เงินติดล้อ จำกัด (Ngern tid lor)
            เลขที่บัญชี
            607-000-0578</p>
        <p>หมายเลขโทรศัพท์ คุณสุภาวดี(061-4107698)คุณชญาณ์นนท์(084-4391662) fax.02-5291395 (เฟสบุ๊ค ngerntidlor)</p>

        <div class="col-12">
            <img src="payment/image4.png" width="100%">
            <br><br>
        </div>
</body>

</div>

</html>