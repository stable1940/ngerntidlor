﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Login V12</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/css/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!--===============================================================================================-->
    <style>
        @media (max-width: 420px) {
            .pad-data {
                padding-left: 0px;
            }
             #invis {
                visibility: hidden;
                clear: both;
                float: left;
                margin: 10px auto 5px 20px;
                width: 28%;
                display: none;
            }
        }

        @media only screen and (min-width: 992px) {
           
        }

        .footer {
            position: relative;
            left: 0;
            bottom: 0;
            width: 100%;

            color: white;
            text-align: center;
        }

    </style>
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headOff.jsp" %>

<body style="font-size: 13px">
    <br>
    <div class="container">
        <div class="row">
            <div class="form-check border-shadow " style="width: 90%">


                <form action="checkvehicle" method="post">
                    <span class="p-t-20 p-b-45">
                        <h3 style="padding: 10px; text-align:center">
                            ตรวจสภาพรถ
                        </h3>
                    </span>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <input class="register-input" type="text" name="keyword"
                                    placeholder="เลขที่สัญญา หรือ เลขทะเบียน" value="${keyword}">

                            </div>

                            <div class="col-md-4" style="margin: auto; text-align: center">
                                <button class="myBtn" onclick="form1.submit();" style="width: 100%">
                                    ค้นหา
                                </button>
                            </div>
                        </div>
                    </div>








                </form>
            </div>
        </div>
    </div>

    <c:if test="${vehicle.getVehicleBrand()!=null}">
        <br><br>
        <form name="form2" class="container border-shadow " action="savevehicle" method="post"
            style="text-align: center">
            <div class="container border-shadow" style="text-align: center">
                <div class="row col-left">
                    <div class="col-4 col-border">
                        ยี่ห้อ
                    </div>
                    <div class="col-4 col-border">
                        รุ่น
                    </div>
                    <div class="col-4 col-border">
                        ทะเบียน
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getVehicleBrand()}" />
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getVehicleModel()}" />
                    </div>
                    <div class="col-4 col-border">
                        <c:out value="${vehicle.getRegisteration()}" />
                    </div>
                </div>
            </div>

            <br><br>
            <div class="container border-shadow" style="text-align: center">
                <div class="row col-left">
                    <div class="col-4 col-border">
                        หัวข้อ
                    </div>
                    <div class="col-4 col-border">
                        ข้อมูล
                    </div>
                    <div class="col-4 col-border">
                        ความถูกต้อง
                    </div>
                    <div class="col-4 col-border">
                        เลขที่สัญญา
                    </div>
                    <div class="col-4 col-border pad-data">
                        <c:out value="${vehicle.getAgreementNo()}" />
                    </div>
                    <div class="col-4 col-border checkbox-mid">
                        <input type="checkbox" name="chk_AgreementNo" <c:if
                            test="${chkVehicle.getChk_AgreementNo() == '1'}">checked="checked"
    </c:if> >
    <span class="checkmark"></span>
    </div>
    <div class="col-4 col-border">
        เลขเครื่องยนต์
    </div>
    <div class="col-4 col-border pad-data">
        <c:out value="${vehicle.getAUTO_ENGINE_NO()}" />
    </div>
    <div class="col-4 col-border checkbox-mid">
        <input type="checkbox" name="chk_auto_engine_no" <c:if
            test="${chkVehicle.getChk_auto_engine_no() == '1'}">checked="checked"
        </c:if>>
        <span class="checkmark"></span>
    </div>
    <div class="col-4 col-border">
        เลขตัวถัง
    </div>
    <div class="col-4 col-border pad-data">
        <c:out value="${vehicle.getAUTO_BODY_NO()}" />
    </div>
    <div class="col-4 col-border checkbox-mid">
        <input type="checkbox" name="chk_auto_body_no" <c:if
            test="${chkVehicle.getChk_auto_body_no() == '1'}">checked="checked"
        </c:if>>
        <span class="checkmark"></span>
    </div>
    </div>
    </div>
    <br><br>
    <div class="container border-shadow" style="text-align: center">
        <div class="row col-left">
            <div class="col-4 col-border">
                สภาพ
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="condition_vehicle" value="1" <c:if
                        test="${chkVehicle.getCondition_vehicle() == '1'}">checked="checked"
                    </c:if> required>
                    <span class="radiomark"></span>เดิม
                </label>
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="condition_vehicle" value="2" <c:if
                        test="${chkVehicle.getCondition_vehicle() == '2'}">checked="checked"
                    </c:if>>
                    <span class="radiomark"></span>ไม่เดิม
                </label>
            </div>
            <div class="col-4 col-border">
                ล้อ
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="wheel" value="1" <c:if
                        test="${chkVehicle.getWheel() == '1'}">checked="checked"
                    </c:if> required>
                    <span class="radiomark"></span>ล้อแม็ก
                </label>
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="wheel" value="2" <c:if
                        test="${chkVehicle.getWheel() == '2'}">checked="checked"
                    </c:if> >
                    <span class="radiomark"></span>ล้อลวด
                </label>
            </div>
            <div class="col-4 col-border">
                เบรคหน้า
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="frontbreak" value="1" <c:if
                        test="${chkVehicle.getFrontbreak() == '1'}">checked="checked"
                    </c:if> required>
                    <span class="radiomark"></span>ดิส
                </label>
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="frontbreak" value="2" <c:if
                        test="${chkVehicle.getFrontbreak() == '2'}">checked="checked"
                    </c:if>>
                    <span class="radiomark"></span>ดรั้ม
                </label>
            </div>
            <div class="col-4 col-border">
                เบรคหลัง
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="backbreak" value="1" <c:if
                        test="${chkVehicle.getBackbreak() == '1'}">checked="checked"
                    </c:if> required>
                    <span class="radiomark"></span>ดิส
                </label>
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="backbreak" value="2" <c:if
                        test="${chkVehicle.getBackbreak() == '2'}">checked="checked"
                    </c:if>>
                    <span class="radiomark"></span>ดรั้ม
                </label>
            </div>
            <div class="col-4 col-border">
                ครัช
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="clutch" value="1" <c:if
                        test="${chkVehicle.getClutch() == '1'}">checked="checked"
                    </c:if> required>
                    <span class="radiomark"></span>มี
                </label>
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="clutch" value="2" <c:if
                        test="${chkVehicle.getClutch() == '2'}">checked="checked"
                    </c:if>>
                    <span class="radiomark"></span>ไม่มี
                </label>
            </div>
            <div class="col-4 col-border">
                สตาร์ท
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="start" value="1" <c:if
                        test="${chkVehicle.getStart() == '1'}">checked="checked"
                    </c:if> required>
                    <span class="radiomark"></span>มือ
                </label>
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="start" value="2" <c:if
                        test="${chkVehicle.getStart() == '2'}">checked="checked"
                    </c:if>>
                    <span class="radiomark"></span>เท้า
                </label>
            </div>
            <div class="col-4 col-border">
                กุญแจ
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="vehicle_key" value="1" <c:if
                        test="${chkVehicle.getVehicle_key() == '1'}">checked="checked"
                    </c:if> required>
                    <span class="radiomark"></span>มี
                </label>
            </div>
            <div class="col-4 col-border">
                <label class="container checkbox-mid">
                    <input type="radio" name="vehicle_key" value="2" <c:if
                        test="${chkVehicle.getVehicle_key() == '2'}">checked="checked"
                    </c:if>>
                    <span class="radiomark"></span>ไม่มี
                </label>
            </div>




        </div>
    </div>

    <br><br>

    <div class="container border-shadow">

        <div class="row" style="padding-top: 20px">
            <div class="col-4" style="margin:auto;">
                หมายเหตุ
            </div>
            <div class="col-8">
                <label> <textarea style="width: 100%; height: 90%" rows="4" cols="50" type="text" name="conditionreport"
                        placeholder="หมายเหตุ">${chkVehicle.getConditionReport()}</textarea>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                ลำดับเข้าขาย
            </div>
            <div class="col-8">
                <label><input class="input100" type="number" name="ordersell" placeholder="ลำดับเข้าขาย"
                        value="${chkVehicle.getOrdersell()}">
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                ราคาประเมิน
            </div>
            <div class="col-8">
                <label><input class="input100" type="number" name="pricestart" placeholder="ราคาประเมิน"
                        value="${chkVehicle.getPricestart()}">
                </label>
            </div>
        </div>
        <div style="text-align: center">
            <!-- <button class="myBtn" style="width: 100px" onclick="form2.submit();">
                บันทึก
            </button> -->
            <input type="submit" class="myBtn" style="width: 100px" value="บันทึก">
            <input type="hidden" name="ID" value="${chkVehicle.getId()}">
            <input type="hidden" name="vehicleid" value="${vehicle.getID()}">
            </form>

        </div>
        </c:if>
    </div>
    </div>
    <div id="invis" style="height: 424px">

    </div>

    <%@ include file="footer.jsp" %>
</body>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script>
<script src="/LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/LoginNgerntidlorvendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<!-- <script src="LoginNgerntidlor/js/main.js"></script> -->


</html>
