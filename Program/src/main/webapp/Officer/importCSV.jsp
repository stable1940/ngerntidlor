﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <div w3-include-html="../include/link.jsp"></div>

    <title>upload</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/css/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!--===============================================================================================-->
    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;

            color: white;
            text-align: center;
        }

    </style>

    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headOff.jsp" %>

<body style="font-size: 13px">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">


                <form class="form-check border-shadow" action="upload" method="post">
                    <span class="p-t-20 p-b-45">
                        <h3 style="padding: 10px; text-align:center">
                            นำเข้าข้อมูลรถ
                        </h3>

                    </span>

                </form>
            </div>
        </div>
    </div>


    <br><br>
    <div class="container border-shadow" style="text-align: center;padding: 5%; width: 90%;">




        <form class="form-check" method="post" action="importcsv" enctype="multipart/form-data">

            <div style="padding:5px; color:red;font-style:italic;">
                ${errorMessage}
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="m-b-10" style="text-align: center;" data-validate="Username is required">
                        <a href="examplecsv\vehicle_example.csv" download>ตัวอย่างไฟล์ excel</a>

                    </div>
                    <!-- type file must have name -->
                    <label><input class="btn btn-primary" type="file" name="filecsv" accept=".csv" style="width: 90%">
                    </label>
                </div>
            </div>
            </br>

            <br>

            <input type="hidden" name="ID" value="${vehicle.getID()}">
            <input type="hidden" name="vehicle_id" value="${chkVehicle.getId()}">

            <div style="text-align: center"><input class="btn btn-primary" type="submit" value="Upload" /></div>
            <br>

        </form>

    </div>
    </div>
    <%@ include file="footer.jsp" %>
</body>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="LoginNgerntidlor/vendor/bootstrap/js/popper.js"></script>
<script src="/LoginNgerntidlorvendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/LoginNgerntidlorvendor/select2/select2.min.js"></script>
<!--===============================================================================================-->



</html>
