﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');

    </style>

    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <style>
        * {
            box-sizing: border-box
        }

        body {
            font-family: Verdana, sans-serif;
            margin: 0
        }

        .mySlides {
            display: none
        }

        img {
            vertical-align: middle;
        }

        /* Slideshow container */
        .slideshow-container {
            max-width: 1000px;
            position: relative;
            margin: auto;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -22px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: none;
        }

        /* Caption text */
        .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
            cursor: pointer;
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            transition: background-color 0.6s ease;
        }

        .active,
        .dot:hover {
            background-color: #717171;
        }

        /* Fading animation */
        .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 1.5s;
            animation-name: fade;
            animation-duration: 1.5s;
        }

        @-webkit-keyframes fade {
            from {
                opacity: .4
            }

            to {
                opacity: 1
            }
        }

        @keyframes fade {
            from {
                opacity: .4
            }

            to {
                opacity: 1
            }
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {

            .prev,
            .next,
            .text {
                font-size: 11px
            }
        }

    </style>
    <%@ include file="navbarInclude.jsp" %>
</head>
<%@ include file="headCustomer.jsp" %>

<body>
    <div class="container form-check border-shadow" style="max-width: 500px;">
        <div class="row">


            <form name="form1" class="form-register " action="list" method="get">
                <input type="hidden" name="page" value="${page}">
                <span>
                    <h5 style="text-align:center">
                        <c:out value="${vehicle.getVehicleBrand()}" />
                        <c:out value="${vehicle.getVehicleModel()}" />
                    </h5>
                </span>

            </form>

            <div class="container">

                <div class="row">
                    <div class="col-12">
                        <div class="slideshow-container">
                            <c:forEach var="pic" items="${pics}">
                                <div class="mySlides ">
                                    <img src="${pic}" width="100%" height="300px">
                                </div>
                            </c:forEach>
                            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                            <a class="next" onclick="plusSlides(1)">&#10095;</a>
                        </div>
                        <div>
                            <div style="text-align: center">
                                <c:forEach var="pic" items="${pics}">
                                    <span class="dot"></span>

                                </c:forEach>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="typo-section col-6">เลขที่สัญญา</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getAgreementNo()}" />
                    </div>

                    <div class="typo-section col-6">วันที่ยึด</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getRepodate()}" />
                    </div>


                    <div class="typo-section col-6">สถานะข้อมูล</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getProcessDescThai()}" />
                    </div> -->

                    <div class="typo-section col-6">เลขที่เครื่องยนต์</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getAUTO_ENGINE_NO()}" />
                    </div>

                    <div class="typo-section col-6">เลขที่ตัวถัง</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getAUTO_BODY_NO()}" />
                    </div>

                    <div class="typo-section col-6">เชื้อเพลิง</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getENGINE_TYPE_DESC()}" />
                    </div>

                    <div class="typo-section col-6">Mile(km.)</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getPhysicalMile()}" />
                    </div>

                    <div class="typo-section col-6">สี</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getVehicleColor()}" />
                    </div>

                    <div class="typo-section col-6">ทะเบียน</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getRegisteration()}" />
                    </div>

                    <div class="typo-section col-6">จังหวัดทะเบียน</div>
                    <div class="typo-content col-6">
                        <c:out value="${vehicle.getRegisterationProvince()}" />
                    </div>

                    <div class="typo-section col-6">หมายเหตุ</div>
                    <div class="typo-content col-6">
                        <c:out value="${chkVehicle.getConditionReport()}" />
                    </div>


                    <div class="typo-section col-6">
                        <p id="demo"></p>
                    </div>
                    <div class="typo-content col-6">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
    </div>

</body>
<script>
    // Set the date we're counting down to
    var time_begin = new Date("${setTime.getDate()},${setTime.getTime_begin()}").getTime();
    var time_end = new Date("${setTime.getDate()},${setTime.getTime_end()}").getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        if (time_begin > now) {
            var distance = time_begin - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            var str = " การประมูลจะเริ่มใน ";
            if (days > 0) str += days + "วัน ";
            if (hours > 0) str += hours + "ชั่วโมง ";
            if (minutes > 0) str += minutes + "นาที ";
            if (seconds > 0) str += seconds + "วินาที ";

            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = str;
        } else if (time_end > now) {

            var distance = time_end - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            var str = " การประมูลจะจบใน ";
            if (days > 0) str += days + "วัน ";
            if (hours > 0) str += hours + "ชั่วโมง ";
            if (minutes > 0) str += minutes + "นาที ";
            if (seconds > 0) str += seconds + "วินาที ";

            // Output the result in an element with id="demo"
            str += "<br/><button class='myBtn' onClick=\"window.location.href='auction?id=${vehicle.getID()}'\">เข้าประมูล</button>";
            document.getElementById("demo").innerHTML = str;
        } else {
            clearInterval(x);
            str = "หมดเวลาประมูล";
            document.getElementById("demo").innerHTML = str;
        }
        // If the count down is over, write some text 
    }, 1000);

    // slide show
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) { slideIndex = 1 }
        if (n < 1) { slideIndex = slides.length }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";

    }
</script>

</html>
