﻿<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Index</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
   
<style>
    @media (max-width: 420px) {
            .form-register {
                width: 90%;
            }

            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: red;
                color: white;
                text-align: center;
            }

            .height-fix {
                height: 80px;
            }

            .btn-size {
                height: 50px;
                width: 350px;
            }
        }

        @media (width: 320px) and (height: 568px) {
            .form-register {}
        }

        @media only screen and (min-width: 600px) {
            .form-register {
                width: 90%;

            }

            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: red;
                color: white;
                text-align: center;
            }
        }

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
            .form-register {
                width: 500px;
            }

            .footer {
                position: fixed;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1030;
            }


            .pad-top {
                padding-top: 50px;
            }

            .btn-size {
                height: 100px;
                width: 250px;
            }

            .height-fix {
                height: 200px;
            }




        }
</style>
<%@ include file="navbarInclude.jsp" %>

</head>
<nav class="navbar navbar-expand-md navbar-dark navbar-fixed-top" style="background-color: #0060ab; font-size:16px;">
    <a class="navbar-brand" href="index"><img
            src="https://www.ngerntidlor.com/NTL/media/NTL-Library/Logo/logo-ntl-2018-rgb@2x.png" width="120px"></a>

    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">

        </ul>
    </div>
</nav>

<div class="pad-top">

</div>

</div>

<body>
    <div class="form-register border-shadow" style="text-align: center;  margin: auto">

        
        <form>
            <div>
                <input class="myBtn" type="button" value="นำเข้าข้อมูลรถ" onclick="window.location.href='importcsv'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="ตรวจสภาพรถ" onclick="window.location.href='checkvehicle'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="ดาวน์โหลดข้อมูลตรวจสภาพรถ"
                    onclick="window.location.href='/exportcsv2'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="อัพโหลดรูปรถเข้าประมูลออนไลน์"
                    onclick="window.location.href='upload'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="ตั้งเวลาประมูล" onclick="window.location.href='settime'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="ปฏิทินการประมูล" onclick="window.location.href='calendar'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="แคตตาล็อครถ" onclick="window.location.href='listoff'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="ใบเสร็จ" onclick="window.location.href='receipt'" />
            </div>
            <br>
            <div>
                <input class="myBtn" type="button" value="ลงชื่อออก" onclick="window.location.href='logout'" />
            </div>
        </form>




    </div>

    

    <!-- <div class="container">
        <div class="row">
            <div class="col-md-4 height-fix">
                <div class="btn-size">
                    <input class="myBtn" style="width: 100%; height: 100%; border-radius: 10px; text-align: center"
                        type="button" value="นำเข้าข้อมูลรถ" onclick="window.location.href='importcsv'" />
                </div>
            </div>
            <div class="col-md-4 height-fix">
                <div class="btn-size">
                    <input class="myBtn" style="width: 100%; height: 100%; border-radius: 10px; text-align: center"
                        type="button" value="ตรวจสภาพรถ" onclick="window.location.href='checkvehicle'" />
                </div>
            </div>
            <div class="col-md-4 height-fix">
                <div class="btn-size">
                    <input class="myBtn"
                        style="width: 100%; height: 100%; border-radius: 10px; text-align: center;cursor: pointer;"
                        value="ดาวน์โหลดข้อมูลตรวจสภาพรถ"
                    onclick="window.location.href='/exportcsv2'" />
                </div>
            </div>
            <div class="col-md-4 height-fix" style=" text-align: center">
                <div class="btn-size">
                    <input class="myBtn"
                        style="width: 100%; height: 100%; border-radius: 10px; text-align: center;cursor: pointer;"
                        value="อัพโหลดรูปรถเข้าประมูลออนไลน์"
                    onclick="window.location.href='upload'" />
                </div>
            </div>
            <div class="col-md-4 height-fix" style="text-align: center">
                <div class="btn-size ">
                    <input class="myBtn" style="width: 100%; height: 100%; border-radius: 10px; text-align: center;"
                        type="button" value="ตั้งเวลาประมูล" onclick="window.location.href='settime'" />
                </div>
            </div>
    <div class="col-md-4 height-fix" style="text-align: center">
        <div class="btn-size ">
            <input class="myBtn" style="width: 100%; height: 100%; border-radius: 10px; text-align: center;" type="button"
                value="ปฏิทินการประมูล" onclick="window.location.href='calendar'" />
        </div>
    </div>
    <div class="col-md-4 height-fix" style="text-align: center">
        <div class="btn-size ">
            <input class="myBtn" style="width: 100%; height: 100%; border-radius: 10px; text-align: center;" type="button"
                value="แคตตาล็อครถ" onclick="window.location.href='listoff'" />
        </div>
    </div>
    <div class="col-md-4 height-fix" style="text-align: center">
        <div class="btn-size ">
            <input class="myBtn" style="width: 100%; height: 100%; border-radius: 10px; text-align: center;" type="button"
                value="ใบเสร็จ" onclick="window.location.href='receipt'" />
        </div>
    </div>
    <div class="col-md-4 height-fix" style="text-align: center">
        <div class="btn-size ">
            <input class="myBtn" style="width: 100%; height: 100%; border-radius: 10px; text-align: center;" type="button"
                value="ลงชื่อออก" onclick="window.location.href='logout'" />
        </div>
    </div>
        </div>
    </div> -->

    <br>
<%@ include file="footer.jsp" %>

    <script src="js/main.js"></script>
   

</body>

</html>
