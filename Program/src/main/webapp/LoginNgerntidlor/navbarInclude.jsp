<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="css/styles.css">


<style>
    * {
        font-family: kanit;
    }

    a:hover {
        background-color: #007bff;
    }

    .footer {

        left: 0;
        bottom: 0;
        width: 100%;
        background-color: #0060ab;
        color: white;
        text-align: center;

    }

    .icon {
        width: 30px;
    }

    .ga:hover {
        opacity: 0.7;
    }
</style>