﻿<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Register</title>
    <style>
        .checkBtn {

            background-color: #0060ab;
            border-color: #0060ab;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 1rem;
            padding: 0.375rem 0.75rem;
            color: white;
        }

        @media only screen and (min-width: 600px) {

            body {
                
            }

            #form1 {
                font-size: 30px;

            }

            .form-register {

                width: 90%;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: red;
                color: white;
                text-align: center;
            }
        }

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
            

            #form1 {
                font-size: 16px;

            }

            .form-register {
                width: 500px;
            }
            .footer{
                text-align: center;
                position: relative;
            }
        }



        @import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');

    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
   
    <script language="javascript">
        function checkID(id) {
            if (id.length != 13) return false;
            for (i = 0, sum = 0; i < 12; i++)
                sum += parseFloat(id.charAt(i)) * (13 - i); if ((11 - sum % 11) % 10 != parseFloat(id.charAt(12)))
                return false; return true;
        }

        function checkForm() {
            if (!checkID($("#idcard").val())) {
                $("#idcard").focus();
                alert('รหัสประชาชนไม่ถูกต้อง');
                return false;
            }
            if ($("#password").val() != $("#cfpassword").val()) {
                $("#cfpass").html("กรุณาใส่รหัสผ่านให้ตรงกัน")
                return false;

            }
        }
    </script>
<%@ include file="navbarInclude.jsp" %>
</head>
<nav class="navbar navbar-expand-md navbar-dark navbar-fixed-top" style="background-color: #0060ab; font-size:16px;">
    <a class="navbar-brand" href="index"><img
            src="https://www.ngerntidlor.com/NTL/media/NTL-Library/Logo/logo-ntl-2018-rgb@2x.png" width="120px"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            
        </ul>
    </div>
</nav>

<body>
    <div style="padding: 20px">

        <form name="form1" onsubmit="return checkForm();" action="register" method="post">
    </div>
    <div class="form-register border-shadow">
        <div class="form-input">
            ชื่อจริง:
            <input class="register-input" type="text" name="firstname" id="firstname" required
                placeholder="กรุณากรอกชื่อ" oninvalid="this.setCustomValidity('กรุณากรอกชื่อจริง')"
                oninput="this.setCustomValidity('')" value="${firstname}" />
        </div>
        <div class="form-input">
            นามสกุล:
            <input class="register-input" type="text" name="lastname" id="lastname" required
                placeholder="กรุณากรอกนามสกุล" oninvalid="this.setCustomValidity('กรุณากรอกนามสกุล')"
                oninput="this.setCustomValidity('')" value="${lastname}" />
        </div>
        <div class="form-input">
            <!-- <input class="register-input" type="number" name="idcard" id="idcard"
                        placeholder="กรุณากรอกรหัสประจำตัวประชาชน 13 หลัก"> -->
            รหัสประจำตัวประชาชน:
            <input class="register-input" type="number" name="idcard" id="idcard" required
                placeholder="กรุณากรอกรหัสประจำตัวประชาชน"
                oninvalid="this.setCustomValidity('กรุณากรอกรหัสประจำตัวประชาชน')" oninput="this.setCustomValidity('')"
                value="${idcard}" />
        </div>
        <div class="form-input">
            เบอร์โทรศัพท์:
            <input class="register-input" type="number" name="tel" id="tel" required
                placeholder="กรุณากรอกเบอร์โทรศัพท์" oninvalid="this.setCustomValidity('กรุณากรอกเบอร์โทรศัพท์')"
                oninput="this.setCustomValidity('')" value="${tel}" />
        </div>
        <div class="form-input">
            อีเมลล์:
            <input class="register-input" type="email" name="email" required placeholder="กรุณากรอกอีเมลล์"
                oninvalid="this.setCustomValidity('กรุณากรอกอีเมลล์ให้ถูกต้อง')" oninput="this.setCustomValidity('')"
                value="${email}" />
        </div>
        <div class="form-input">
            ที่อยู่:
            <input class="register-input" type="text" name="address" required placeholder="กรุณากรอกที่อยู่"
                oninvalid="this.setCustomValidity('กรุณากรอกที่อยู่')" oninput="this.setCustomValidity('')"
                value="${address}" />
        </div>
        <div class="form-input">
            ไอดี Line:
            <input class="register-input" type="text" name="line" required placeholder="กรุณากรอกไอดี Line"
                oninvalid="this.setCustomValidity('กรุณากรอกไอดี Line')" oninput="this.setCustomValidity('')"
                value="${line}" />
        </div>
        <div class="form-input">
            ชื่อร้าน:
            <input class="register-input" type="text" name="storename" required placeholder="กรุณากรอกชื่อร้าน"
                oninvalid="this.setCustomValidity('กรุณากรอกชื่อร้าน')" oninput="this.setCustomValidity('')"
                value="${storename}" />
        </div>
        <div class="form-input">

            ชื่อผู้ใช้:
            <input class="register-input" type="text" name="username" required placeholder="กรุณากรอกชื่อผู้ใช้"
                oninvalid="this.setCustomValidity('กรุณากรอกชื่อผู้ใช้')" oninput="this.setCustomValidity('')"
                value="${username}" />
            <p style="color:red;">
                <c:out value="${output}" />
            </p>
        </div>
        <div class="form-input">
            รหัสผ่าน:
            <input class="register-input" type="password" name="password" id="password" required
                placeholder="กรุณากรอกรหัสผ่าน" oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่าน')"
                oninput="this.setCustomValidity('')" value="${password}" />
        </div>
        <div class="form-input">
            ยืนยันรหัสผ่าน:
            <input class="register-input" type="password" name="cfpassword" id="cfpassword" required
                placeholder="กรุณากรอกรหัสผ่านอีกครั้ง" oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่านอีกครั้ง')"
                oninput="this.setCustomValidity('')" value="${cfpassword}" />
            <p id="cfpass" style="color:red;"></p>
        </div>
        <div class="btn-center">
            <input type="submit" class="myBtn" value="ยืนยัน">
        </div>
        <div class="btn-center">
            <a href="index" class="myBtn">
                กลับ
            </a>
        </div>
        </form>
    </div>
<br>
    <%@ include file="footer.jsp" %>
</body>
<script>
    var check = function () {
        if (document.getElementById('password').value ==
            document.getElementById('cfpassword').value) {
            document.getElementById('message').style.color = 'green';
            document.getElementById('message').innerHTML = 'matching';
        } else {
            document.getElementById('message').style.color = 'red';
            document.getElementById('message').innerHTML = 'not matching';
        }
    }
</script>

</html>
