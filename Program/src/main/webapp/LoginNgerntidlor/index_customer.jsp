﻿<!DOCTYPE html>
<html lang="en">

<head>
    <div w3-include-html="../include/link.jsp"></div>
    <title>Index</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="LoginNgerntidlor/images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="LoginNgerntidlor/css/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!--===============================================================================================-->
    <style>
        @media (max-width: 420px) {
            .form-register {
                width: 90%;
            }

            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: red;
                color: white;
                text-align: center;
            }

            .height-fix {
                height: 80px;
            }

            .btn-size {
                height: 50px;
                width: 350px;
            }
        }

        @media (width: 320px) and (height: 568px) {
            .form-register {}
        }

        @media only screen and (min-width: 600px) {
            .form-register {
                width: 90%;

            }

            .pad-top {
                padding-top: 20px;
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: red;
                color: white;
                text-align: center;
            }
        }

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
            .form-register {
                width: 500px;
            }

            .footer {
                position: fixed;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1030;
            }


            .pad-top {
                padding-top: 100px;
            }

            .btn-size {
                height: 100px;
                width: 200px;
            }

            .height-fix {
                height: 200px;
            }



        }

    </style>
    <%@ include file="navbarInclude.jsp" %>
</head>

<body style="text-align: center">
    <nav class="navbar navbar-expand-md navbar-dark navbar-fixed-top"
        style="background-color: #0060ab; font-size:16px;">
        <a class="navbar-brand" href="index"><img
                src="https://www.ngerntidlor.com/NTL/media/NTL-Library/Logo/logo-ntl-2018-rgb@2x.png" width="120px"></a>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">

            </ul>
        </div>
    </nav>
    <div class="pad-top">
        <div>



            <div id="div-wid" class="form-register border-shadow" style="text-align: center;">

                
                <h6>
                    ${output}
                </h6>
                <span>
                    เมนู
                </span><br><br>

                <form>
                    <div>
                        <input class="myBtn" type="button" value="แก้ไขข้อมูล"
                            onclick="window.location.href='profile'" />
                    </div>
                    <br>
                    <div>
                        <input class="myBtn" type="button" value="แคตตาล็อครถ ออนไลน์"
                            onclick="window.location.href='list'" />
                    </div>
                    <br>
                    <div>
                        <input class="myBtn" type="button" value="รายการประมูลทั้งหมด"
                            onclick="window.location.href='history'" />
                    </div>
                    <br>
                    <div>
                        <input class="myBtn" type="button" value="ปฏิทินการประมูล"
                            onclick="window.location.href='showcalendar'" />
                    </div>

                    <br>
                    <div>
                        <input class="myBtn" type="button" value="ลงชื่อออก" onclick="window.location.href='logout'" />
                    </div>
                </form>


            </div>

            </form>
        </div>
    </div>

            <!-- <div class="container">
                <div class="row">
                    <div class="col-md-3 height-fix">
                        <div class="btn-size">
                            <input class="myBtn"
                                style="width: 100%; height: 100%; border-radius: 10px; text-align: center" type="button"
                                value="แก้ไขข้อมูล" onclick="window.location.href='profile'" />
                        </div>
                    </div>
                    <div class="col-md-3 height-fix">
                        <div class="btn-size">
                            <input class="myBtn"
                                style="width: 100%; height: 100%; border-radius: 10px; text-align: center" type="button"
                                value="แคตตาล็อครถ ออนไลน์" onclick="window.location.href='list'" />
                        </div>
                    </div>
                    <div class="col-md-3 height-fix">
                        <div class="btn-size">
                            <input class="myBtn"
                                style="width: 100%; height: 100%; border-radius: 10px; text-align: center;cursor: pointer;"
                                value="รายการประมูลทั้งหมด" onclick="window.location.href='history'" />
                        </div>
                    </div>
                    <div class="col-md-3 height-fix" style=" text-align: center">
                        <div class="btn-size">
                            <input class="myBtn"
                                style="width: 100%; height: 100%; border-radius: 10px; text-align: center;cursor: pointer;"
                                value="ปฏิทินการประมูล" onclick="window.location.href='showcalendar'" />
                        </div>
                    </div>
                    <div class="col-md-3 height-fix" style="text-align: center">
                        <div class="btn-size ">
                            <input class="myBtn"
                                style="width: 100%; height: 100%; border-radius: 10px; text-align: center;"
                                type="button" value="ลงชื่อออก" onclick="window.location.href='logout'" />
                        </div>
                    </div>

                </div>
            </div> -->

            <%@ include file="footer.jsp" %>





</body>

</html>
